﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GkAnket.Controllers
{
    public class KisiListesiController : Controller
    {
        // GET: KisiListesi
        public ActionResult Index()
        {
            var anketler = new List<GkAnket.Models.Kisilistesi>();

            anketler.Add(new Models.Kisilistesi
            {
                Id = Guid.NewGuid(),
                UyeAdi = "Mehmet Enes",
                UyeSoyadi = "Aktas",
                Bolumu = "Bilişim",
                Sinifi = 3,



            });

            anketler.Add(new Models.Kisilistesi
            {
                Id = Guid.NewGuid(),
                UyeAdi = "Ebru Zeynep",
                UyeSoyadi = "Nair",
                Bolumu = "Bilgisayar",
                Sinifi = 3,

            });
            return View(anketler);
        }
    }
}