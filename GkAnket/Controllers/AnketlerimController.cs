﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GkAnket.Controllers
{
    public class AnketlerimController : Controller
    {
        public ActionResult Index()
        {

            var anketler = new List<GkAnket.Models.AnketItemModel>();

            anketler.Add(new Models.AnketItemModel {
                Id = Guid.NewGuid(),
                AnketAdi = "Anket 1",
                Bekleyen = 10,
                Cevaplayan = 120


            });

            anketler.Add(new Models.AnketItemModel
            {
                Id = Guid.NewGuid(),
                AnketAdi = "Anket 2",
                Bekleyen =70,
                Cevaplayan = 40


            });

            return View(anketler);
        }

        

        
    }
}