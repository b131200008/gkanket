﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GkAnket.Controllers
{
    public class CevaplanacakAnketlerimController : Controller
    {
        // GET: CevaplanacakAnketlerim
        public ActionResult Index()
        {
            var anketler = new List<GkAnket.Models.CevaplanacakAnketlerim>();

            anketler.Add(new Models.CevaplanacakAnketlerim { 
                Id = Guid.NewGuid(),
                AnketAdi = "Gönüllük Eğitimi",
                OlusturulanTarih = "10.12.1016",

            });

            anketler.Add(new Models.CevaplanacakAnketlerim
            {
                Id = Guid.NewGuid(),
                AnketAdi = "Afad Eğitimi",
                OlusturulanTarih = "10.10.2016",

            });
            return View(anketler);
        }
    }
}