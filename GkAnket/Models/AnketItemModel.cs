﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GkAnket.Models
{
    public class AnketItemModel
    {
        
        public Guid Id { get; set; }

        [DisplayName("Anket Adı")]
        public string AnketAdi { get; set; }

        [DisplayName("Cevaplayan Sayısı")]
        public int Cevaplayan { get; set; }

        [DisplayName("Cevaplanmamış Sayısı")]
        public int Bekleyen { get; set; }

        [DisplayName("ToplamKisi Sayısı")]
        public int ToplamKisi { get { return (this.Bekleyen + this.Cevaplayan);  } }
    }
}