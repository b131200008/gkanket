﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;


namespace GkAnket.Models
{
    public class CevaplanacakAnketlerim
    {
        public Guid Id { get; set; }

        [DisplayName("AnketAdı")]
        public string AnketAdi { get; set; }

        [DisplayName("Oluşturlan Tarih")]
        public string OlusturulanTarih { get; set; }


    }
}