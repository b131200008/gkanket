﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GkAnket.Models
{
    public class Kisilistesi
    {
        public Guid Id { get; set; }

        [DisplayName("Uye Adı")]
        public string UyeAdi { get; set; }

        [DisplayName("Uye Soyadı")]
        public string UyeSoyadi { get; set; }

        [DisplayName("Bölümü")]
        public string Bolumu { get; set; }

        [DisplayName("Sınıfı")]
        public int Sinifi { get; set; }

    }
}